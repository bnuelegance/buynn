extends KinematicBody2D

signal touch_down
signal touch_up
signal drag_start
signal drag
signal drag_end
signal tap
signal long_press

const MAX_TAP_TIME := 400

var _intends_to_drag := false
var _is_dragging := false
var _start_counter := 0.0
var _pressed_position := Vector2.ZERO
var _velocity := Vector2.ZERO

func _process(_delta: float):
	if _is_dragging:
		var mouse_pos := get_global_mouse_position()
		global_position = mouse_pos
		emit_signal("drag")
	elif _intends_to_drag:
		_velocity = Vector2.ZERO
		var mouse_pos := get_global_mouse_position()
		var dist := _pressed_position.distance_to(mouse_pos)
		if dist > 1:
			_is_dragging = true
			emit_signal("drag_start")


func _emit_drag_change(pressed: bool):
	_velocity = Vector2.ZERO
	if _intends_to_drag == pressed:
		return
	if pressed:
		_intends_to_drag = true
		_is_dragging = false
		_start_counter = OS.get_ticks_msec()
		_pressed_position = get_global_mouse_position()
		emit_signal("touch_down")
	else:
		if _is_dragging:
			emit_signal("drag_end")
		else:
			var elapsed = OS.get_ticks_msec() - _start_counter
			if elapsed < MAX_TAP_TIME:
				emit_signal("tap")
			else:
				emit_signal("long_press")
		_intends_to_drag = false
		_is_dragging = false
		emit_signal("touch_up")


func _input(event: InputEvent) -> void:
	if event is InputEventMouseButton:
		var evt := event as InputEventMouseButton 
		_emit_drag_change(evt.pressed)
	if event is InputEventMouseMotion:
		var evt := event as InputEventMouseMotion
		_velocity = evt.speed 


func _ready() -> void:
	for part in get_children():
		if part is Line2D:
			var line := part as Line2D
			var points_amount := line.points.size()
			var stiffness := 500 * points_amount 
			var mass := 10.0
			var damping := 200.0
			for i in line.points.size():
				var point := line.points[i]
				var spring := Spring.new()
				spring.set_initial_position(point)
				spring.stiffness = stiffness
				spring.mass = mass
				spring.damping = damping
				part.add_child(spring)

func _physics_process(delta: float) -> void:
	for part in get_children():
		if part is Line2D:
			var line := part as Line2D
			var v := _velocity
			var i = line.points.size() - 1
			while i >= 0:
				var spring: Spring = line.get_child(i)
				var pos := spring.derive_velocity(v).next_position(delta)
				line.points[i] = pos
				v = spring.velocity
				i-=1
