extends Node2D

var mouse_position := Vector2()
var mouse_press := Vector2()
var mouse_speed := Vector2()

var circle_main := Circle.new()
var circle_second := Circle.new()
var spring := Spring.new()


func _ready() -> void:
	var half_viewport := get_viewport_rect().size / 2.0
	circle_main.radius = 50.0
	circle_main.global_position = half_viewport - Vector2(circle_main.radius, circle_main.radius)
	circle_main.color = Color.red
	add_child(circle_main)
	
	mouse_position = circle_main.global_position
	
	circle_second.radius = 35.0
	var offset := Vector2(circle_second.radius, 0)
	circle_second.global_position = circle_main.position + offset
	circle_second.color = Color.teal
	add_child(circle_second)
	
	
	spring.spring_length = offset * 2
	spring.position = circle_second.global_position


func _input(event: InputEvent) -> void:
	if event is InputEventMouseMotion:
		var evt := event as InputEventMouseMotion
		mouse_position = evt.global_position
	elif event is InputEventMouseButton:
		var evt := event as InputEventMouseButton
		if evt.pressed:
			mouse_press = evt.global_position
			return
		var start_pos := circle_main.global_position
		#var aim_pos := evt.global_position
		#var velocity = aim_pos - start_pos
		var velocity :=  evt.global_position - mouse_press
		spawn(start_pos, velocity)


func spawn(start_pos: Vector2, velocity: Vector2):
	var b := Bullet.new()
	b.radius = 10
	owner.add_child(b)
	b.jump.position = start_pos
	b.jump.velocity = velocity


func _physics_process(delta: float) -> void:
	circle_main.global_position = mouse_position
	var pos := spring.derive_velocity(mouse_position).next_position(delta)
	circle_second.global_position = pos + mouse_position


class Circle extends Node2D:
	
	var radius := 50.0 setget set_radius
	var color := Color(1, 1, 1, 1) setget set_color
	var points := 0.0
	
	func _init(init_pos := position, init_radius := radius, init_color := color):
			position = init_pos
			radius = init_radius
			color = init_color
	
	func _draw() -> void:
		var nb_points := radius / 2.0 if points <= 0 else points
		_draw_circle(Vector2.ZERO, radius, color, nb_points)
	
	
	func _draw_circle(center := Vector2(0,0), circle_radius := 50.0, circle_color := Color(1,1,1,.5), nb_points = 60):
		
		var points_arc := PoolVector2Array()
		
		var angle := deg2rad(360 / nb_points)
		for i in range(nb_points + 1):
			var point_angle := i * angle
			var point_position := Vector2(cos(point_angle), sin(point_angle)) * circle_radius
			points_arc.push_back(center + point_position)

		draw_polygon(points_arc, PoolColorArray([circle_color]))

	func set_radius(new_radius: float) -> void:
		radius = new_radius
		update()

	func set_color(new_color: Color) -> void:
		color = new_color
		update()

class Bullet extends Circle:
	
	var jump: Jump = Jump.new()


	func _physics_process(delta: float) -> void:
		position = jump.next_position(delta)
		if position.y < -1000:
			queue_free()


class Jump:
	var velocity := Vector2.ZERO
	var gravity := Vector2(0, 900)
	var position := Vector2.ZERO
	
	func next_position(delta: float) -> Vector2:
		if position.y >= 1000:
			position.y = 1000
			return position
		velocity += gravity * delta
		position += velocity * delta
		return position
