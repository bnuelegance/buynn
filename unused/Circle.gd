tool
extends Node2D

export var radius := 50.0 setget set_radius
export var color := Color(1, 1, 1, 1) setget set_color


func _draw() -> void:
	var nb_points := radius / 2.0
	_draw_circle(Vector2.ZERO, radius, color, nb_points)
	
	
func _draw_circle(center := Vector2(0,0), circle_radius := 50.0, circle_color := Color(1,1,1,.5), nb_points = 60):
	
	var points_arc := PoolVector2Array()
	
	var angle := deg2rad(360 / nb_points)
	for i in range(nb_points + 1):
		var point_angle := i * angle
		var point_position := Vector2(cos(point_angle), sin(point_angle)) * circle_radius
		points_arc.push_back(center + point_position)

	draw_polygon(points_arc, PoolColorArray([circle_color]))


func set_radius(new_radius: float) -> void:
	radius = new_radius
	update()


func set_color(new_color: Color) -> void:
	color = new_color
	update()
