extends Node
class_name Spring

var position := Vector2.ZERO
var velocity := Vector2.ZERO
var spring_length := Vector2(35, 0)
var stiffness := 2000.0
var mass := 20.0
var damping := 200.0
var offset := Vector2.ZERO

var _previous_point := Vector2.ZERO

func set_initial_position(initial_position: Vector2 = Vector2.ZERO):
	position = initial_position
	spring_length = initial_position

func derive_velocity(the_next_point: Vector2) -> Spring:
	velocity += _previous_point - the_next_point
	_previous_point = the_next_point
	return self

func next_position(delta: float) -> Vector2:
	var frame_spring := -stiffness * (position - spring_length);
	var frame_damping := -damping * velocity;
	var acceleration := (frame_spring + frame_damping) / mass;
	velocity += acceleration * delta;
	position += velocity * delta;
	return position + offset
