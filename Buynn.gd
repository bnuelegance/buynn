extends KinematicBody2D

signal touched_floor
signal jumped

var speed := 100
var jump_speed := -1800
var gravity := 400

var velocity := Vector2.ZERO
var mouse_position := Vector2.ZERO
var direction_is_right := false
var _was_on_floor := false


onready var bnuy := $Bnuy as Node2D
onready var bnuy_eye_open := bnuy.get_node("EyeOpen") as Node2D
onready var bnuy_eye_close := bnuy.get_node("EyeClose") as Node2D
const SfxrStreamPlayer := preload("res://addons/godot_sfxr/SfxrStreamPlayer.gd")
onready var player := $SfxrStreamPlayer as SfxrStreamPlayer

func _ready() -> void:
	# warning-ignore:return_value_discarded
	connect("jumped", self, "_on_jumped")
	# warning-ignore:return_value_discarded
	connect("touched_floor", self, "_on_touched_floor")
	
	for part in bnuy.get_children():
		if part is Line2D:
			var line := part as Line2D
			var points_amount := line.points.size()
			var stiffness := 300 *  points_amount
			var mass := 30.0
			var damping := 200.0
			for i in line.points.size():
				var point := line.points[i]
				var spring := Spring.new()
				spring.set_initial_position(point)
				spring.stiffness = stiffness
				spring.mass = mass
				spring.damping = damping
				part.add_child(spring)



func update_target(to_global_position: Vector2) -> void:
	mouse_position = to_global_position


func request_jump() -> bool:
	if is_on_floor():
		_was_on_floor = false
		velocity = to_local(mouse_position)
		jumped()
		emit_signal("jumped")
		return true
	return false

func get_direction() -> Vector2:
	var x := - sign(velocity.x) if velocity.x != 0 else 1.0
	return Vector2(x, 1)


func _physics_process(delta: float) -> void:
	velocity.y += gravity * delta
	velocity = move_and_slide(velocity, Vector2.UP)
	bnuy.scale = get_direction()
	for part in bnuy.get_children():
		if part is Line2D:
			var line := part as Line2D
			var v := velocity
			for i in line.points.size():
				var spring: Spring = line.get_child(i)
				var pos := spring.derive_velocity(v).next_position(delta)
				line.points[i] = pos
				v = spring.velocity
	if is_on_floor():
		if not _was_on_floor:
			touched_floor()
	elif position.y > 2000:
		get_parent()._on_fell()


var thread: Thread

func touched_floor():
	_was_on_floor = true
	emit_signal("touched_floor")
	player.play()


func jumped():
	if OS.get_name() != 'HTML5': # no threads in web?
		if thread and thread.is_active():
			thread.wait_to_finish()
		thread = Thread.new()
		# warning-ignore:return_value_discarded
		thread.start(self, "_generate_sound")


func _generate_sound():
	player.set('generators/hit', true)
	player.build_sfx(false)


func _exit_tree():
	if thread and thread.is_active():
		thread.wait_to_finish()

func _process(_delta):
	update_bnuy_eyes()

func update_bnuy_eyes():
	var body = bnuy.get_node("Body") as Line2D
	# first point is the head position
	var head_pos := body.points[0] as Vector2
	bnuy_eye_open.position = head_pos
	bnuy_eye_close.position = head_pos

func _on_jumped():
	bnuy_eye_close.visible = true
	bnuy_eye_open.visible = false

func _on_touched_floor():
	bnuy_eye_close.visible = false
	bnuy_eye_open.visible = true

"""
Dreams Become Real by Kevin MacLeod
Link: https://incompetech.filmmusic.io/song/3678-dreams-become-real
License: https://filmmusic.io/standard-license
"""
