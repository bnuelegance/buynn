extends StaticBody2D
class_name BnyuLevel

# warning-ignore:unused_signal
signal up
# warning-ignore:unused_signal
signal right
# warning-ignore:unused_signal
signal down
# warning-ignore:unused_signal
signal left

var down: BnyuLevel
var up: BnyuLevel

func _ready():
	collision_layer = 10
	collision_mask = 1
	solidify_lines()
	add_limits()

func solidify_lines():
	for child in get_children():
		if child is Line2D and child.visible:
			var line := child as Line2D
			line.width = 40
			line.joint_mode = Line2D.LINE_JOINT_BEVEL
			line.begin_cap_mode = Line2D.LINE_CAP_ROUND
			line.end_cap_mode = Line2D.LINE_CAP_ROUND
			line.width_curve = preload("./BunLevelCurve.tres")
			for polygon in Geometry.offset_polyline_2d(line.points, line.width/2, Geometry.JOIN_ROUND, Geometry.END_ROUND):
				var collision_polygon = CollisionPolygon2D.new()
				collision_polygon.position = line.position + Vector2(0, line.width/3)
				collision_polygon.set_deferred("one_way_collision", true)
				collision_polygon.polygon = polygon
				call_deferred("add_child", collision_polygon)

func add_limits():
	var vecs := [Vector2.UP, Vector2.RIGHT, Vector2.DOWN, Vector2.LEFT]
	var names := ["up", "right", "down", "left"]
	var margins := [Vector2(0, 500), Vector2(50, 0), Vector2(0, 100), Vector2(50, 0)]
	for i in 4:
		var area := GeometryHelper.make_screen_limit(vecs[i], margins[i])
		call_deferred('add_child', area)
		# warning-ignore:return_value_discarded
		area.connect("body_entered", self, "_on_entered", [names[i]])
	#GeometryHelper.make_screen(self)

func _on_entered(_body: Node, signal_name: String) -> void:
	emit_signal(signal_name)


func append(new_level: BnyuLevel):
	var old_level := self
	var offset := (get_viewport_rect().size * Vector2(0, 1))
	var target_position := global_position - offset
	new_level.global_position = target_position - offset / 2
	var tween := Tween.new()
	new_level.add_child(tween)
	# warning-ignore:return_value_discarded
	tween.interpolate_property(new_level, "global_position",
		new_level.global_position, target_position, 1,
		Tween.TRANS_BOUNCE, Tween.EASE_OUT)
	# warning-ignore:return_value_discarded
	tween.connect("tween_all_completed", tween, "queue_free")
	# warning-ignore:return_value_discarded
	tween.start()
	new_level.down = old_level
	old_level.up = new_level
	if old_level.down:
		if old_level.down.down:
			old_level.down.down.queue_free()
