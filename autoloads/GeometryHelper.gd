extends Node2D

var debug_color := Color.red

func make_screen(parent: Node2D, viewport_area :=  get_viewport_rect().size):
	debug_color = Color.red
	parent.add_child(
		fill_area(
			viewport_area, 
			Vector2(300, 200), 
			Rect2(100, 100, 100, 100),
			0.1
		)
	)

	debug_color = Color.orange
	parent.add_child(
		fill_area(
			viewport_area, 
			Vector2(400, 400), 
			Rect2(100, 50, 50, 30),
			0.1
		)
	)

	debug_color = Color.blue
	parent.add_child(
		fill_area(
			viewport_area, 
			Vector2(200, 200), 
			Rect2(10,10,50,50), 
			0.5
		)
	)


func fill_area(area: Vector2, block_size: Vector2, variance: Rect2, chances_to_skip := 0.1) -> StaticBody2D:
	var rng: RandomNumberGenerator = Globals.rand;
	var steps = area/block_size
	var body := StaticBody2D.new()
	for x in steps.x:
		for y in steps.y:
			var should_draw =  (x + y) % 2 == 0 and rng.randf() > chances_to_skip
			if not should_draw: 
				continue
			var pos := Vector2(x, y) * block_size
			var pol := make_collision_object(Rect2(pos, block_size), variance, 3.0)
			body.add_child(pol)
	return body


func make_collision_object(bounding_box: Rect2, variance: Rect2, thickness := 30.0) -> CollisionPolygon2D:
	var rng: RandomNumberGenerator = Globals.rand;	
	var nmb_points := rng.randi_range(2, 5)
	var width := rng.randf_range(bounding_box.size.x - variance.size.x, bounding_box.size.x + variance.size.x);
	var position := Vector2(
		rng.randf_range(bounding_box.position.x - variance.position.x, bounding_box.position.x + variance.position.x),
		rng.randf_range(bounding_box.position.y - variance.position.y, bounding_box.position.y + variance.position.y)
	)
	var polygon := PoolVector2Array()
	var offset := []
	var x_delta := width / nmb_points
	var i := 0
	while i <= nmb_points:
		var x := x_delta * i 
		var y := rng.randf_range(bounding_box.size.y - variance.size.y, bounding_box.size.y + variance.size.y);
		var point := Vector2(x, y)
		polygon.append(point)
		var offset_point := Vector2(x, y + thickness)
		offset.append(offset_point)
		i += 1
	var line := Line2D.new()
	line.points = polygon
	line.modulate = debug_color
	var collision_polygon = CollisionPolygon2D.new()
	offset.invert()
	polygon.append_array(offset)
	collision_polygon.polygon = polygon
	collision_polygon.one_way_collision = true
	collision_polygon.add_child(line)
	collision_polygon.position = position
	return collision_polygon

static func offset_line(points: PoolVector2Array, thickness := 30.0) -> PoolVector2Array:  
	var pol := PoolVector2Array()
	var offset := []
	for i in points.size():
		var p := points[i]
		pol.append(p)
		offset.append(Vector2(p.x, p.y + thickness))
	offset.invert()
	pol.append_array(offset)
	return pol


func points_to_collision_polygon(points: PoolVector2Array, thickness := 30.0) -> CollisionPolygon2D:
	var collision_polygon = CollisionPolygon2D.new()
	collision_polygon.polygon = offset_line(points, thickness)
	return collision_polygon

func solidify_line(line: Line2D) -> CollisionPolygon2D:
		var collision_polygon = points_to_collision_polygon(line.points)
		collision_polygon.position = line.position
		collision_polygon.set_deferred("one_way_collision", true)
		return collision_polygon


func make_screen_limit(side: Vector2, margins := Vector2(50, 100)) -> Area2D:
	
	var screen := get_viewport_rect().size / 2
	margins = margins / 2
	var area := Area2D.new()
	area.collision_layer = 0
	area.collision_mask = 1
	
	var collision_shape := CollisionShape2D.new()
	area.add_child(collision_shape)
	
	var shape := RectangleShape2D.new()
	collision_shape.shape = shape
	match side:
		Vector2.UP:
			area.name = "up"
			shape.extents = Vector2(screen.x, margins.y)
			collision_shape.position = Vector2(screen.x, margins.y)
		Vector2.DOWN:
			area.name = "down"
			shape.extents = Vector2(screen.x * 4, margins.y)
			collision_shape.position = Vector2(screen.x, screen.y * 2 - margins.y)
		Vector2.LEFT:
			area.name = "left"
			shape.extents = Vector2(margins.x, screen.y)
			collision_shape.position = Vector2(-margins.x, screen.y)
		Vector2.RIGHT:
			area.name = "right"
			shape.extents = Vector2(margins.x, screen.y)
			collision_shape.position = Vector2(screen.x * 2 + margins.x, screen.y)
		_:
			push_error("the f?")
	return area
