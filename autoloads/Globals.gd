extends Node

signal points_changed
signal game_over

var rand := RandomNumberGenerator.new()

var points := 0 setget set_points

func set_points(new_points: int) -> void:
	points = new_points
	emit_signal("points_changed")

func game_over():
	emit_signal("game_over")
