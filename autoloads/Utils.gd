class_name Utils

const FILE_TYPE_SCENE := "\\.tscn"


static func find_scenes_in_directory(directory_path: String) -> PoolStringArray:
	return find_files_in_directory(directory_path, FILE_TYPE_SCENE)


static func find_files_in_directory(directory_path: String, file_type: String) -> PoolStringArray:
	var files := PoolStringArray()
	var directory := Directory.new()

	var can_continue := directory.open(directory_path) == OK
	if not can_continue:
		print_debug('Could not open directory "%s"' % [directory_path])
		return files

	var regex := RegEx.new()
	# warning-ignore:return_value_discarded
	regex.compile(file_type)

	
	var ok = directory.list_dir_begin(true, true)
	if ok != OK:
		return files
	var file_name := directory.get_next()
	while file_name != "":
		if not directory.current_is_dir() and regex.search(file_name):
			files.append(directory_path.plus_file(file_name))
		file_name = directory.get_next()

	return files


static func generate_paths_file(files: PoolStringArray) -> String:
	var consts = PoolStringArray()
	var arr = PoolStringArray()
	var obj = PoolStringArray()
	var regex = RegEx.new()
	regex.compile("^\\d+")
	for file in files:
		file = file as String
		var name: String = file.get_file().get_basename()
		if regex.search(name):
			name = "File_%s"%[name]
		consts.append("const %s = preload('%s');"%[name, file])
		arr.append("preload('%s')"%[file])
		obj.append("'%s': preload('%s')"%[name, file])
	var file_source = """
%s

const map = {
	%s
}

const list = [
	%s
]
"""%[consts.join("\n"), obj.join(",\n\t"), arr.join(",\n\t")]
	return file_source


static func create_paths_file(directory_path: String, file_type: String, cache_file_path: String):
	var paths := find_files_in_directory(directory_path, file_type)
	var file_contents := generate_paths_file(paths)
	var file = File.new()
	file.open(cache_file_path, File.WRITE)
	file.store_string(file_contents)
	file.close()


static func load_resources(paths: Array) -> Array:
	var resources := []
	for path in paths:
		resources.append(load(path))
	return resources


static func get_random_element_from_array(array: Array):
	return array[randi() % array.size()]
