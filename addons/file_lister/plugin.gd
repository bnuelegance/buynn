tool
extends EditorPlugin

var button := ToolButton.new()


func _enter_tree() -> void:
	button.icon = get_editor_interface().get_base_control().get_icon("File", "EditorIcons")
	button.hint_tooltip = "cache the directory"
	button.connect("pressed", self, "_on_pressed")
	add_control_to_container(CONTAINER_TOOLBAR, button)
	button.get_parent().move_child(button, button.get_index() - 2)


func _exit_tree() -> void:
	remove_control_from_container(CONTAINER_TOOLBAR, button)
	button.queue_free()


func _on_pressed():
	var dir = "res://levels"
	var file = "res://levels/dir_cache.gd"
	Utils.create_paths_file(dir, Utils.FILE_TYPE_SCENE, file)
