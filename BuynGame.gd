extends Control

const Bell := preload("res://BynBell.gd")
const Bnuy := preload("res://Buynn.gd")
const Trajectory = preload("res://BunTrajectory.gd")
const Level0 = preload("res://Level_00.tscn")

onready var prediction_line := $BnuTrajectory as Trajectory
onready var bell := $BnyBell as Bell
onready var bynu := $Bnuuuuy as Bnuy
onready var levels_container := $Levels as Node
onready var points_label := $CanvasLayer/Control/PointsLabel as Label

var _touched_floor_once := false
const levels := preload("res://levels/dir_cache.gd").list;
var _level: BnyuLevel
var _initial_position := Vector2.ZERO
var _last_ground_position: Vector2


func _ready() -> void:
	# warning-ignore:return_value_discarded
	bell.connect("drag", self, "_on_bell_dragged")
	# warning-ignore:return_value_discarded
	bell.connect("tap", self, "_on_bell_tapped")
	# warning-ignore:return_value_discarded
	bell.connect("drag_end", self, "_on_bell_tapped")
	
	var bnuuy := bynu
	_initial_position = bynu.global_position
	# warning-ignore:return_value_discarded
	bnuuy.connect("touched_floor", self, "_on_touched_floor")
	_on_bell_dragged()
	_on_fell(true)
	# warning-ignore:return_value_discarded
	Globals.connect("points_changed", self, "_on_points_changed")
	_on_points_changed()

func _on_points_changed():
	points_label.text = "%s"%[Globals.points]

func _on_bell_dragged() -> void:
	var bnny := bynu
	var pos := bell.global_position
	bnny.update_target(pos)


func _on_bell_tapped() -> void:
	var bnyu = bynu
	if bnyu.request_jump():
		prediction_line.fade_out()


func _on_touched_floor() -> void:
	prediction_line.fade_in()
	var buyun := bynu
	if _touched_floor_once and buyun.global_position.y < _last_ground_position.y:
		_reached_higher(buyun.global_position)
	if not _touched_floor_once:
		_touched_floor_once = true
		_last_ground_position = buyun.global_position
		# warning-ignore:return_value_discarded
		_level.connect("up", self, "_reached_top", [_level])
		_reached_top(_level)

func _reached_higher(pos: Vector2):
	var delta := int(abs(_last_ground_position.y - pos.y) / 10)
	Globals.points += delta
	_last_ground_position = pos

func _process(delta: float) -> void:
	var nub := bynu
	var pos := nub.global_position
	var vel = nub.to_local(bell.global_position)
	prediction_line.update_trajectory(pos, vel, nub.gravity, delta)


func load_random_level(PackedLevel: PackedScene = null) -> BnyuLevel:
	if PackedLevel == null:
		PackedLevel = Utils.get_random_element_from_array(levels) as PackedScene
	var level: BnyuLevel = PackedLevel.instance()
	levels_container.add_child(level)
	# warning-ignore:return_value_discarded
	return level


func _reached_top(old_level: BnyuLevel):
	old_level.disconnect("up", self, "_reached_top")
	var new_level := load_random_level()
	new_level.connect("up", self, "_reached_top", [new_level])
	old_level.append(new_level)
	# warning-ignore:return_value_discarded
	old_level.connect("down", self, "_on_fell")


func _on_fell(first_time := false):
	Globals.points = 0
	Globals.game_over()
	_last_ground_position = get_viewport_rect().size
	
	if not first_time:
		yield(get_tree().create_timer(1), "timeout")
	for child in levels_container.get_children():
		child.queue_free()
	var bnyuu := bynu
	bnyuu.global_position = _initial_position
	_level = load_random_level(Level0)
	_touched_floor_once = false
	

