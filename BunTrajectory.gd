extends Line2D


var tween := Tween.new()
var _opacity := 1.0


func _init() -> void:
	add_child(tween)

func _ready() -> void:
	# warning-ignore:return_value_discarded
	Globals.connect("points_changed", self, "_on_points_changed");
	# warning-ignore:return_value_discarded
	Globals.connect("game_over", self, "_reset")


func _reset():
	_opacity = 1.0


func _on_points_changed():
	var points: int = Globals.points
	var factor := max(float(points) / 100.0, 1)
	_opacity = max(min(_opacity / factor, 1), 0)
	if not tween.is_active():
		modulate.a = _opacity


func fade(out := true):
	var from := modulate
	var to := modulate
	to.a = 0.0 if out else _opacity
	var duration := 0.3 if out else 0.2
	var transition := Tween.TRANS_LINEAR
	var easing := Tween.EASE_IN if out else Tween.EASE_OUT
	# warning-ignore:return_value_discarded
	tween.stop_all()
	# warning-ignore:return_value_discarded
	tween.interpolate_property(self, "modulate", from, to, duration, transition, easing)
	# warning-ignore:return_value_discarded
	tween.start()



func fade_in():
	fade(false)


func fade_out():
	fade(true)


func update_trajectory(pos: Vector2, vel:Vector2, gravity: float, delta: float) -> void:
	if not visible:
		return
	clear_points()
	for i in 100:
		add_point(pos)
		vel.y += gravity * delta
		pos += vel * delta
